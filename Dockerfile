FROM ubuntu:20.04
#
# install dependencies and clean up afterwards
RUN apt update && \
    apt install -y --no-install-recommends ca-certificates bzip2 curl python procps x11vnc xvfb libxcursor1 libnss3 libegl1-mesa libasound2 libglib2.0-0 libxcomposite-dev less jq && \
    apt -q clean all && \
    rm -rf /tmp/* /var/tmp/*


WORKDIR /opt/sinusbot

ADD install.sh .
RUN chmod 755 install.sh

# # Download/Install SinusBot
# RUN bash install.sh sinusbot

# Download/Install youtube-dl
RUN bash install.sh youtube-dl

# # Configure config.ini
# RUN bash install.sh configure

# Download/Install TeamSpeak Client
RUN bash install.sh teamspeak

# ADD entrypoint.sh .
# RUN chmod 755 entrypoint.sh

# EXPOSE 8087

# VOLUME ["/opt/sinusbot/data", "/opt/sinusbot/scripts"]

# ENTRYPOINT ["/opt/sinusbot/entrypoint.sh"]
